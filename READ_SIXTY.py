from ConfigReader import pooledconnection
from DBAccessor import DBAccesor
from datetime import datetime, timedelta
import pandas as pd



class Read_Sixty():

    def __init__(self):
        self._DBA = DBAccesor()

    # Returns list of active HARP Nnumber
    def activeharp(self):
        sql = """SELECT `maintable`.`HarpNumber`, `maintable`.`StartTime`
        FROM `maintable`
        WHERE `maintable`.`IsActive` = 1"""

        list = pooledconnection.execute(sql)
        return list

    def twelvehoursbefore(self):
        twelvehoursbefore = datetime.now() - timedelta(hours=12)
        return twelvehoursbefore

    # This method checks if the HARP Number has been active for atleast 12 hours,
    # So we would ideally have enough data to process.
    def checkharpactivetime(self, harpstarttime):
        harpstarttime = harpstarttime + timedelta(hours=0)
        if self.twelvehoursbefore() > harpstarttime:
            return 1
        else:
            return 0

    def retrievecalculatedvalues(self, integer):
        sql = """SELECT *
        FROM `calculatedvalues`
        WHERE `calculatedvalues`.`HarpNumber` = %s
        ORDER BY `calculatedvalues`.`ObsStart` DESC
        LIMIT 60"""
        print("Method Accessed")
        # print(harpnumber)
        # print(type(harpnumber))
        # y = harpnumber
        # print(type(y))
        # z = str(harpnumber)
        # print(type(z))

        calculatedvalues = pooledconnection.execute(sql, (integer,))
        print("SQL Executed")
        # calculatedvalues = pooledconnection.execute(sql, (1234,))
        return (calculatedvalues)

    def interpolate(self, list):

        values = []
        length = len(list)
        for i in range(0, length):
            listz = list[i][0]
            df = pd.DataFrame(listz)
            df[3] = df[3].interpolate()
            df[4] = df[4].interpolate()
            df[5] = df[5].interpolate()
            df[6] = df[6].interpolate()
            #     print(df)
            tempdf = df[[3, 4, 5, 6]]
            latesttimestamp = list[i][0][0][2]
            harpnumber = list[i][0][0][1]
            values.append(tempdf)
            values.append(latesttimestamp)
            values.append(harpnumber)

        return values







    def mainprocess(self, harpnumber):
        integer = harpnumber
        calculatedvalues = self.retrievecalculatedvalues(integer)  # List of 60 values

        latesttimestamp1 = calculatedvalues[0][2]
        latesttimestamp = latesttimestamp1 + timedelta(hours=0)  # Time of latest value

        timestampslist = []  # This is the list of timestamps for the past 60 files, including missing ones
        temptimestamp = latesttimestamp

        for x in range(0, 60):
            timestampslist.append(temptimestamp)
            temptimestamp = temptimestamp - timedelta(minutes=12)

        # Declare empty list which will contain missing timestamp values:
        listofmissingvalues = []

        # Final List of calculated values:
        finallist = []

        counter = 0
        for i in range(0, 60):
            if (timestampslist[i] == calculatedvalues[counter][2]):
                finallist.append(calculatedvalues[counter])
                counter = counter + 1
            else:
                listofmissingvalues.append(timestampslist[i])
                finallist.append(None)

        return finallist, listofmissingvalues

    def maincall(self):
        activeharpnumbers = self.activeharp()
        length = len(activeharpnumbers)

        listactiveharpnumbers = []

        for i in range(0, length):
            listactiveharpnumbers.append(activeharpnumbers[i][0])

        output = []
        for i in range(0, length):
            if (self.checkharpactivetime(activeharpnumbers[i][1]) == 1):
                temp = int(activeharpnumbers[i][0])
                print(temp)
                output.append(self.mainprocess(temp))

        #At this point, the output is ready.
        #However, may have missing values, thus may need to interpolae.

        #Prints HarpNum set of values
        # print(output[2][0])


        # #Prints LocalTracker 8 things.
        # print(output[0][0][0])
        #
        # #Prints Time Stamp
        # print(output[1][0][0][2])
        #
        # print(activeharpnumbers)
        # print(len(output))

        values = self.interpolate(output)


        return values


####README
# To run, call the maincall method. It will itself query the database for active regions,
# check if there is enough data (i.e. past 12 hours = 60 values)
# and if yes, it will output for each active harp number with enough data:
# array = [[localtracker, harpnumber, datetime, USFLUX, MEANGBZ, R_VALUE, FDIM, B_EFF],[LIST OF MISSING TIME VALUES]]
# So, you can extract array[0], and from that only keep what you need.
#Which can be added as a method which cleans up the arrays before they are returned in mainprocess **
# array[1] is the list of missing timestamps.
# If we interpolate I don't think this will be necessary, it is provided however.



RS = Read_Sixty()
print(RS.maincall())
