from ConfigReader import pooledconnection
import configparser


class DBCreator(object):

    def __init__(self):
        configfilepath = "C:\\Users\\piyus\\PycharmProjects\\nrt-harp-data-processing\\config.ini"
        config = configparser.ConfigParser()
        # config.read('config.ini')
        config.read(configfilepath)
        cdatabase = config['DATABASE']['database']

        self._database = cdatabase
        self._cmecactus = '''CREATE TABLE `CME_CACTUS` (
  `Issue_Date` datetime DEFAULT NULL,
  `CME` bigint DEFAULT NULL,
  `t0` datetime DEFAULT NULL,
  `dt0` bigint DEFAULT NULL,
  `pa` bigint DEFAULT NULL,
  `da` bigint DEFAULT NULL,
  `v` bigint DEFAULT NULL,
  `dv` bigint DEFAULT NULL,
  `minv` bigint DEFAULT NULL,
  `maxv` bigint DEFAULT NULL,
  `halo?` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

        self._integralelectrons = '''CREATE TABLE `integral-electrons` (
  `time_tag` datetime DEFAULT NULL,
  `satellite` bigint DEFAULT NULL,
  `>=2 MeV` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

        self._integralelectronsbackup = '''CREATE TABLE `integral-electrons-backup` (
  `time_tag` datetime DEFAULT NULL,
  `satellite` bigint DEFAULT NULL,
  `flux` double DEFAULT NULL,
  `energy` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

        self._integralprotons = '''CREATE TABLE `integral-protons` (
  `time_tag` datetime DEFAULT NULL,
  `satellite` bigint DEFAULT NULL,
  `>=1 MeV` double DEFAULT NULL,
  `>=10 MeV` double DEFAULT NULL,
  `>=100 MeV` double DEFAULT NULL,
  `>=30 MeV` double DEFAULT NULL,
  `>=5 MeV` double DEFAULT NULL,
  `>=50 MeV` double DEFAULT NULL,
  `>=500 MeV` double DEFAULT NULL,
  `>=60 MeV` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

        self._integralprotonsbackup = '''CREATE TABLE `integral-protons-backup` (
  `time_tag` datetime DEFAULT NULL,
  `satellite` bigint DEFAULT NULL,
  `flux` double DEFAULT NULL,
  `energy` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

        self._xrayflares = '''
CREATE TABLE `xray-flares` (
  `time_tag` timestamp NULL DEFAULT NULL,
  `begin_time` timestamp NULL DEFAULT NULL,
  `begin_class` text,
  `max_time` timestamp NULL DEFAULT NULL,
  `max_class` text,
  `max_ratio` double DEFAULT NULL,
  `max_ratio_time` timestamp NULL DEFAULT NULL,
  `current_int_xrlong` double DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `end_class` text,
  `satellite` double DEFAULT NULL,
  `NOAA_AR` text,
  `latitude` text,
  `longitude` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

        self._xrayflaresbackup = '''CREATE TABLE `xray-flares-backup` (
  `time_tag` timestamp NULL DEFAULT NULL,
  `satellite` double DEFAULT NULL,
  `begin_time` timestamp NULL DEFAULT NULL,
  `begin_class` text,
  `max_time` timestamp NULL DEFAULT NULL,
  `max_class` text,
  `current_int_xrlong` double DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `max_ratio_time` text,
  `max_ratio` text,
  `end_class` text,
  `NOAA_AR` text,
  `latitude` text,
  `longitude` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

    def checkdatabaseexists():
        # Note: test schema to be replaced from the config file for production version.
        databaselist = pooledconnection.execute("SHOW DATABASES")
        if "test" in str(databaselist):
            return True
        else:
            return False

    def checktableexists(tablename):
        # Returns boolean whether table exists in schema
        tablelist = pooledconnection.execute("SHOW TABLES")
        if tablename in str(tablelist):
            return True
        else:
            return False

    def createtable(tablename, sqlcode):
        # Checks whether table exists, if not creates it.
        if DBCreator.checktableexists(tablename) is True:
            pass
        else:
            pooledconnection.execute(sqlcode, commit=True)
        return None

    def deletetable(tablename):
        # Checks whether table exists, if then deletes.
        pooledconnection.execute("DROP TABLE IF EXISTS " + tablename, commit=True)
        return None

    def main(self):

        DBCreator.createtable("CME_CACTUS", self._cmecactus)
        DBCreator.createtable("integral-electrons", self._integralelectrons)
        DBCreator.createtable("integral-electrons-backup", self._integralelectronsbackup)
        DBCreator.createtable("integral-protons", self._integralprotons)
        DBCreator.createtable("integral-protons-backup", self._integralprotonsbackup)
        DBCreator.createtable("xray-flares", self._xrayflares)
        DBCreator.createtable("xray-flares-backup", self._xrayflaresbackup)


DBC = DBCreator()
DBC.main()