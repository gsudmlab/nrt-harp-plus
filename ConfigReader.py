# This class will read from the config file, and also create the DBPool object to be passed on later.

import mysql.connector
from mysql.connector import Error, CMySQLConnection, MySQLConnection, pooling
import configparser
import os

# config file path
configfilepath = "C:\\Users\\piyus\\PycharmProjects\\nrt-harp-data-processing\\config.ini"
config = configparser.ConfigParser()
# config.read('config.ini')
config.read(configfilepath)

#Read Values from config file path
chost = config['DATABASE']['host']
cdatabase = config['DATABASE']['database']
cuser = config['DATABASE']['user']
cpassword = config['DATABASE']['password']
cport2 = config['DATABASE']['port']
cpool_size2 = config['DATABASE']['pool_size']
cport = int(cport2)
cpool_size = int(cpool_size2)


class MySQLPool(object):

    def __init__(self, host=chost, port=cport, user=cuser, password=cpassword, database=cdatabase,
                 pool_size=cpool_size):
        #Sets class attributes
        vals = {} #Dictionary to store values
        self._host = host
        self._user = user
        self._password = password
        self._database = database
        self._port = port

        #Creates the dictionary vals, to create dbconfig
        vals["host"] = self._host
        vals["user"] = self._user
        vals["password"] = self._password
        vals["database"] = self._database
        vals["port"] = self._port
        self.dbconfig = vals
        #initializes the pool
        self.pool = self.create_pool(pool_size=pool_size)

    def create_pool(self, pool_size =3):
        pool = mysql.connector.pooling.MySQLConnectionPool(
            pool_size = pool_size,
            **self.dbconfig)
        return pool

    def close(self, conn, cursor):
        conn.close()
        cursor.close()

    def execute(self, sql, args=None, commit = False):
        conn = self.pool.get_connection()
        cursor = conn.cursor()
        if args:
            cursor.execute(sql, args)
            # self.close(conn, cursor)
        else:
            cursor.execute(sql)
            # self.close(conn, cursor)
        if commit is True:
            conn.commit()
            self.close(conn,cursor)
            return None
        else:
            list = cursor.fetchall()
            self.close(conn,cursor)
            return list

    def executemany(self, sql, args, commit=False):
        conn = self.pool.get_connection()
        cursor = conn.cursor
        cursor.executemany(sql,args)
        if commit is True:
            conn.commit()
            self.close(conn,cursor)
            return None
        else:
            list = cursor.fetchall()
            self.close(conn,cursor)
            return list

# instance of the class which will be called by other modules.
pooledconnection = MySQLPool()